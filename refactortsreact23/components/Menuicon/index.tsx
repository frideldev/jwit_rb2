import styled from 'styled-components'

const Container = styled.div`
width: 430px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`
export const Icon = styled.img`
height: 25px;
width: 25px;
object-fit: contain;
border-radius: 50%;
`
interface params{
    optionIcon:{cart:string,menu:string,search:string}
}
export const Menuicon = (params:params) => {
  return (
    <Container>
    {
        [params.optionIcon.cart,params.optionIcon.menu,params.optionIcon.search].map((v,i)=>{
         return(<Icon key={i} src={v} ></Icon>)
        })
    }
    </Container>
  )
}
