import styled from 'styled-components'
import { Cardproduct } from './Cardproduct'

const Container=styled.div`
width: 430px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`
interface params{
optionCard:{subtitle:string,img:string}[]
}
const Scrollcard = (params:params):JSX.Element => {
   

  return (
    <Container>
    
     {
        params.optionCard.map((v,i)=><Cardproduct key={i} {...v}/>)
      }
    </Container>
  )
}

export default Scrollcard