import styled from 'styled-components'
import { Menulista } from './Menulista'
import Scrollcard from './Scrollcard'
const Container=styled.div`
width: 430px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: space-between;
`
interface params{
    optionsMenu:string[];
    optionCard:{subtitle:string,img:string}[];
    colorText:string;
    
}
const Products = (params:params):JSX.Element => {
  return (
    <Container>
     <Menulista colorText={params.colorText} optionMenu={params.optionsMenu} />
    <Scrollcard optionCard={params.optionCard}/>
    </Container>
  )
}

export default Products