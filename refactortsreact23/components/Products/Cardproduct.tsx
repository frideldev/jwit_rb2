import styled from 'styled-components'
const Container=styled.div`
width: 180px;
height: 250px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
`
const Subtitle=styled.p`
font-weight: 200;
font-size: 12px;
`
const Imagecard=styled.img`
border-radius: 15px;
width: 70%;
`
interface params{
    subtitle:string;
    img:string
}
export const Cardproduct = (params:params):JSX.Element => {
  return (
    <Container>
    <Imagecard src={params.img}></Imagecard>
    <Subtitle>
     {params.subtitle}
    </Subtitle>
    </Container>
  )
}
