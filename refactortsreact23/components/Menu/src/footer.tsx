import React from 'react'
import styled from 'styled-components'
import { Btn } from './ui/Btn'
const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 25px;
padding-top: 15px;
padding-bottom: 15px;
border-top: 1px solid #ffff;
`
interface option{
    icon:string;
    onClick:()=>void;
}
export interface params{
    enableReset?:boolean;
    enableCancel?:boolean;
    onReset?:()=>void;
    onAceptar?:()=>void;
    onCancel?:()=>void;
    recursesImages:{checkgreen:string,close:string,refresh:string}
}

export const Footer = (params:params):JSX.Element => {
    const handleReset= ()=>{
        if(typeof params.onReset ==='function') params.onReset()
      }
      const handleOk= ()=>{
        if(typeof params.onAceptar ==='function') params.onAceptar()
      }
      const handleCancel= ()=>{
        if(typeof params.onCancel ==='function') params.onCancel()
      }
      const data:option[]=[
        //params.enableReset?{icon:refresh,onClick:handleReset}:{icon:'',onClick:()=>{}},
        //params.enableCancel?{icon:close,onClick:handleCancel}:{icon:'',onClick:()=>{}},
        {icon:params.recursesImages.checkgreen,onClick:handleOk}
        ];
        params.enableReset?data.push({icon:params.recursesImages.refresh,onClick:handleReset}):null;
        params.enableCancel?data.push({icon:params.recursesImages.close,onClick:handleCancel}):null;

    return (
        <Container>
        {
           data.map((v,i)=><Btn key={i} image={v.icon} onChange={v.onClick}/>)
         }
 
   </Container>
  )
}
Footer.defaultProps={
  enableReset:true,
  enableCancel:true
}