import React from 'react'
import { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Footer } from './footer'
import { Header } from './header'
import { Options,params as Ioption,paramsOnchange} from './options'
const Container=styled.div`
  background-color: ${props=>props.color};
border-radius: 25px;
padding-top: 15px;
padding-left: 25px;

   width: 400px;
   
`
export interface params{
  options:string[],
  color:string,
  onAceptar:(options:paramsOnchange[])=>void,
  onReset?:()=>void,
  onCancel?:()=>void,
  enabledReset?:boolean,
  recursesImages:{checkgreen:string,close:string,refresh:string,checkred:string},
  colorText:string,
  optionMenu:string[],
  visibilidad:boolean,
  titleHeader:string
}
const Menu = (params:params):JSX.Element => {
  const[selected,setSelected]=useState<paramsOnchange[]>([])
  useEffect(()=>{
    var raw:paramsOnchange[]=[]
    params.options.forEach(el=>raw.push({value:el,state:false}))
    setSelected(raw)
  },[params.options])
 
  const handleClick:Ioption['onChange']=(e)=>{
    const copy = [...selected]
    const targetIndex=copy.findIndex(f=>f.value===e.value)
    if(targetIndex>-1){
      copy[targetIndex]=e;
      
    }
    setSelected(copy)

  }
  const handleAceptar=()=>{
   if(typeof params.onAceptar==='function') params.onAceptar(selected)
  }
  const handleReset=()=>{
    var raw:paramsOnchange[]=[]
    params.options.forEach(el=>raw.push({value:el,state:false}))
    setSelected(raw)
   }
   const handleCancel=()=>{
    if(typeof params.onCancel==='function') params.onCancel()
   }
  return (
    <Container color={params.color}>
      <Header titleHeader={params.titleHeader} colorText={params.colorText} visibilidad={params.visibilidad} optionMenu={params.optionMenu}/>
        {
          selected.map((v,i)=><Options recursesImage={params.recursesImages.checkred} key={i} title={v.value} defaultState={v.state} onChange={handleClick}/>)
        }
        <Footer enableReset={params.enabledReset} recursesImages={params.recursesImages} onAceptar={handleAceptar} onReset={handleReset} onCancel={handleCancel}/>
      </Container>
  )
}
Menu.defaultProps={
  options:[
    "hola",
    "hola2",
    "hola3"
  ],
  color:"red",
  colorText:"white"
}

export default Menu