import React from 'react'
import styled from 'styled-components'
import { Menulista } from './ui/Menulista'

const Container = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
border-bottom: 1px solid #ffff;
`
export const Title =styled.p<{colorText:string}>`
color: ${props=>props.colorText};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`
interface params{
colorText:string;
optionMenu:string[];
visibilidad:boolean;
titleHeader:string;
}
export const Header = (params:params) => {
   if(params.visibilidad===true){
    
    return (
    
        <Container>
        <Title colorText={params.colorText} >{params.titleHeader}</Title>
       <Menulista optionMenu={params.optionMenu} colorText={params.colorText}></Menulista>
        </Container>
      )
   }
    
  else return <></>
}
