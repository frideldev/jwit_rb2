import React from 'react'
import styled from 'styled-components'
import { Checkbox } from './ui/Checkbox'

const Container = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
margin-right: 25px;
align-items: center;
`
export const Title =styled.p`
color: ${props=>props.color};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`
export interface paramsOnchange{
  value:string,
  state:boolean
}
export interface params {
title:string;
defaultState:boolean;
onChange:(params:paramsOnchange)=>void; 
recursesImage:string;
}
export const Options = (params:params) => {
  const handleChange =(state:boolean)=>{
    
    if(typeof params.onChange==='function') params.onChange({value:params.title,state})

}
  return (
    <Container>
   <Title color={'white'}>
    {params.title}
   </Title>
   <Checkbox defaultValue={params.defaultState} image={params.recursesImage} onChange={handleChange}/>
    </Container>
  )
}
