import React from 'react'
import styled from 'styled-components'
const Container=styled.div<{visible:boolean}>`
border-radius: 50%;
border: 1px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: ${props=>props.visible?'white':'transparent'};
`
export const Icon = styled.img<{visible:boolean}>`
height: 35px;
width: 35px;
object-fit: contain;
border-radius: 50%;
visibility: ${props=>props.visible?'visible':'hidden'};
`
export interface params{
    image:string;
    defaultValue:boolean;
    onChange?:(value:boolean)=>void;
}

export const Checkbox = (params:params):JSX.Element => {
    const handleChange =()=>{
        if(typeof params.onChange==='function') params.onChange(!params.defaultValue)
    
    }
  return (
    <Container visible={params.defaultValue} onClick={handleChange}>
    <Icon src={params.image} visible={params.defaultValue}/>
   </Container>
  )
}
Checkbox.defaultProps ={
  defaultValue:false
}
