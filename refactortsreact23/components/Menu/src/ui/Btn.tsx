import React from 'react'
import styled from 'styled-components'
const Container=styled.div`
border-radius: 50%;
border: 1px solid white;
height: 60px;
width: 60px;
cursor: pointer;
background-color: white;
margin-left: 15px;
`
export const Icon = styled.img`
height: 60px;
width: 60px;
object-fit: contain;
border-radius: 50%;
`
export interface params{
    image:string;
    onChange?:()=>void;
}

export const Btn = (params:params):JSX.Element => {
    const handleChange =()=>{
        if(typeof params.onChange==='function') params.onChange()
    
    }
  return (
    <Container  onClick={handleChange}>
    <Icon src={params.image} />
   </Container>
  )
}
