import React from 'react'
import styled from 'styled-components'
const Container=styled.div`
display: flex;
flex-direction: row;
align-items: center;
`
export const Title =styled.p<{colorText:string}>`
color: ${props=>props.colorText};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`
interface params{
    optionMenu:string[];
    colorText:string
}

export const Menulista = (params:params) => {
  return (
    <Container>
     {
        params.optionMenu.map((v,i)=>{return(
            <Title colorText={params.colorText} key={i}>
              {v}
            </Title>
        )})
     }
    </Container>
  )
}
