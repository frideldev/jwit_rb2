"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Footer = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Btn_1 = require("./ui/Btn");
const Container = styled_components_1.default.div `
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 25px;
padding-top: 15px;
padding-bottom: 15px;
border-top: 1px solid #ffff;
`;
const Footer = (params) => {
    const handleReset = () => {
        if (typeof params.onReset === 'function')
            params.onReset();
    };
    const handleOk = () => {
        if (typeof params.onAceptar === 'function')
            params.onAceptar();
    };
    const handleCancel = () => {
        if (typeof params.onCancel === 'function')
            params.onCancel();
    };
    const data = [
        { icon: params.recursesImages.checkgreen, onClick: handleOk }
    ];
    params.enableReset ? data.push({ icon: params.recursesImages.refresh, onClick: handleReset }) : null;
    params.enableCancel ? data.push({ icon: params.recursesImages.close, onClick: handleCancel }) : null;
    return (react_1.default.createElement(Container, null, data.map((v, i) => react_1.default.createElement(Btn_1.Btn, { key: i, image: v.icon, onChange: v.onClick }))));
};
exports.Footer = Footer;
exports.Footer.defaultProps = {
    enableReset: true,
    enableCancel: true
};
