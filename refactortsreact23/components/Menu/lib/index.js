"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_2 = require("react");
const styled_components_1 = __importDefault(require("styled-components"));
const footer_1 = require("./footer");
const header_1 = require("./header");
const options_1 = require("./options");
const Container = styled_components_1.default.div `
  background-color: ${props => props.color};
border-radius: 25px;
padding-top: 15px;
padding-left: 25px;

   width: 400px;
   
`;
const Menu = (params) => {
    const [selected, setSelected] = (0, react_2.useState)([]);
    (0, react_2.useEffect)(() => {
        var raw = [];
        params.options.forEach(el => raw.push({ value: el, state: false }));
        setSelected(raw);
    }, [params.options]);
    const handleClick = (e) => {
        const copy = [...selected];
        const targetIndex = copy.findIndex(f => f.value === e.value);
        if (targetIndex > -1) {
            copy[targetIndex] = e;
        }
        setSelected(copy);
    };
    const handleAceptar = () => {
        if (typeof params.onAceptar === 'function')
            params.onAceptar(selected);
    };
    const handleReset = () => {
        var raw = [];
        params.options.forEach(el => raw.push({ value: el, state: false }));
        setSelected(raw);
    };
    const handleCancel = () => {
        if (typeof params.onCancel === 'function')
            params.onCancel();
    };
    return (react_1.default.createElement(Container, { color: params.color },
        react_1.default.createElement(header_1.Header, { titleHeader: params.titleHeader, colorText: params.colorText, visibilidad: params.visibilidad, optionMenu: params.optionMenu }),
        selected.map((v, i) => react_1.default.createElement(options_1.Options, { recursesImage: params.recursesImages.checkred, key: i, title: v.value, defaultState: v.state, onChange: handleClick })),
        react_1.default.createElement(footer_1.Footer, { enableReset: params.enabledReset, recursesImages: params.recursesImages, onAceptar: handleAceptar, onReset: handleReset, onCancel: handleCancel })));
};
Menu.defaultProps = {
    options: [
        "hola",
        "hola2",
        "hola3"
    ],
    color: "red",
    colorText: "white"
};
exports.default = Menu;
