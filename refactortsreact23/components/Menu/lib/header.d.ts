/// <reference types="react" />
export declare const Title: import("styled-components").StyledComponent<"p", any, {
    colorText: string;
}, never>;
interface params {
    colorText: string;
    optionMenu: string[];
    visibilidad: boolean;
    titleHeader: string;
}
export declare const Header: (params: params) => JSX.Element;
export {};
