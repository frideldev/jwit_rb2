"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menulista = exports.Title = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Container = styled_components_1.default.div `
display: flex;
flex-direction: row;
align-items: center;
`;
exports.Title = styled_components_1.default.p `
color: ${props => props.colorText};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`;
const Menulista = (params) => {
    return (react_1.default.createElement(Container, null, params.optionMenu.map((v, i) => {
        return (react_1.default.createElement(exports.Title, { colorText: params.colorText, key: i }, v));
    })));
};
exports.Menulista = Menulista;
