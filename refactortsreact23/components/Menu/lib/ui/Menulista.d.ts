/// <reference types="react" />
export declare const Title: import("styled-components").StyledComponent<"p", any, {
    colorText: string;
}, never>;
interface params {
    optionMenu: string[];
    colorText: string;
}
export declare const Menulista: (params: params) => JSX.Element;
export {};
