/// <reference types="react" />
export declare const Icon: import("styled-components").StyledComponent<"img", any, {
    visible: boolean;
}, never>;
export interface params {
    image: string;
    defaultValue: boolean;
    onChange?: (value: boolean) => void;
}
export declare const Checkbox: {
    (params: params): JSX.Element;
    defaultProps: {
        defaultValue: boolean;
    };
};
