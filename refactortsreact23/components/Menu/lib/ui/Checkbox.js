"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Checkbox = exports.Icon = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Container = styled_components_1.default.div `
border-radius: 50%;
border: 1px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: ${props => props.visible ? 'white' : 'transparent'};
`;
exports.Icon = styled_components_1.default.img `
height: 35px;
width: 35px;
object-fit: contain;
border-radius: 50%;
visibility: ${props => props.visible ? 'visible' : 'hidden'};
`;
const Checkbox = (params) => {
    const handleChange = () => {
        if (typeof params.onChange === 'function')
            params.onChange(!params.defaultValue);
    };
    return (react_1.default.createElement(Container, { visible: params.defaultValue, onClick: handleChange },
        react_1.default.createElement(exports.Icon, { src: params.image, visible: params.defaultValue })));
};
exports.Checkbox = Checkbox;
exports.Checkbox.defaultProps = {
    defaultValue: false
};
