/// <reference types="react" />
export declare const Icon: import("styled-components").StyledComponent<"img", any, {}, never>;
export interface params {
    image: string;
    onChange?: () => void;
}
export declare const Btn: (params: params) => JSX.Element;
