"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Btn = exports.Icon = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Container = styled_components_1.default.div `
border-radius: 50%;
border: 1px solid white;
height: 60px;
width: 60px;
cursor: pointer;
background-color: white;
margin-left: 15px;
`;
exports.Icon = styled_components_1.default.img `
height: 60px;
width: 60px;
object-fit: contain;
border-radius: 50%;
`;
const Btn = (params) => {
    const handleChange = () => {
        if (typeof params.onChange === 'function')
            params.onChange();
    };
    return (react_1.default.createElement(Container, { onClick: handleChange },
        react_1.default.createElement(exports.Icon, { src: params.image })));
};
exports.Btn = Btn;
