"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Header = exports.Title = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Menulista_1 = require("./ui/Menulista");
const Container = styled_components_1.default.div `
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
border-bottom: 1px solid #ffff;
`;
exports.Title = styled_components_1.default.p `
color: ${props => props.colorText};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`;
const Header = (params) => {
    if (params.visibilidad === true) {
        return (react_1.default.createElement(Container, null,
            react_1.default.createElement(exports.Title, { colorText: params.colorText }, params.titleHeader),
            react_1.default.createElement(Menulista_1.Menulista, { optionMenu: params.optionMenu, colorText: params.colorText })));
    }
    else
        return react_1.default.createElement(react_1.default.Fragment, null);
};
exports.Header = Header;
