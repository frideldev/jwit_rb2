/// <reference types="react" />
export interface params {
    enableReset?: boolean;
    enableCancel?: boolean;
    onReset?: () => void;
    onAceptar?: () => void;
    onCancel?: () => void;
    recursesImages: {
        checkgreen: string;
        close: string;
        refresh: string;
    };
}
export declare const Footer: {
    (params: params): JSX.Element;
    defaultProps: {
        enableReset: boolean;
        enableCancel: boolean;
    };
};
