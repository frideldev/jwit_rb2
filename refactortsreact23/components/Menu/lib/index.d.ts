/// <reference types="react" />
import { paramsOnchange } from './options';
export interface params {
    options: string[];
    color: string;
    onAceptar: (options: paramsOnchange[]) => void;
    onReset?: () => void;
    onCancel?: () => void;
    enabledReset?: boolean;
    recursesImages: {
        checkgreen: string;
        close: string;
        refresh: string;
        checkred: string;
    };
    colorText: string;
    optionMenu: string[];
    visibilidad: boolean;
    titleHeader: string;
}
declare const Menu: {
    (params: params): JSX.Element;
    defaultProps: {
        options: string[];
        color: string;
        colorText: string;
    };
};
export default Menu;
