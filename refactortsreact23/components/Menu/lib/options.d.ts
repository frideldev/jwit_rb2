/// <reference types="react" />
export declare const Title: import("styled-components").StyledComponent<"p", any, {}, never>;
export interface paramsOnchange {
    value: string;
    state: boolean;
}
export interface params {
    title: string;
    defaultState: boolean;
    onChange: (params: paramsOnchange) => void;
    recursesImage: string;
}
export declare const Options: (params: params) => JSX.Element;
