"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Options = exports.Title = void 0;
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Checkbox_1 = require("./ui/Checkbox");
const Container = styled_components_1.default.div `
display: flex;
flex-direction: row;
justify-content: space-between;
margin-right: 25px;
align-items: center;
`;
exports.Title = styled_components_1.default.p `
color: ${props => props.color};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`;
const Options = (params) => {
    const handleChange = (state) => {
        if (typeof params.onChange === 'function')
            params.onChange({ value: params.title, state });
    };
    return (react_1.default.createElement(Container, null,
        react_1.default.createElement(exports.Title, { color: 'white' }, params.title),
        react_1.default.createElement(Checkbox_1.Checkbox, { defaultValue: params.defaultState, image: params.recursesImage, onChange: handleChange })));
};
exports.Options = Options;
