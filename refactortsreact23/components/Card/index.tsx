import styled from 'styled-components'

const Container= styled.div<{images:string}>`
   height: 150px;
   width: 350px;
   border-radius:5px;
   background-image: url(${props=>props.images});
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: column;
`
const Title = styled.div`
color: #ff8b60;
text-transform: uppercase;
font-size: 15px;
font-weight: bold;
width: 90%;
text-align: center;
`
const Button = styled.button`
background-color: #f06f37;
text-transform: uppercase;
border: 1px solid #f06f37;
padding: 7px;
padding-left: 25px;
padding-right: 25px;
color: white;
font-weight: bold;
border-radius: 15px;
opacity: 0.8;
cursor: pointer;
`
interface params{
    title:string;
    titlebutton:string;
    image:string;
}
export const Card = (params:params):JSX.Element => {
    return (
      <Container images={params.image}>
       <Title>
          {params.title}
       </Title>
       <Button>
             {params.titlebutton}
       </Button>
      </Container>
    )
  }