import styled from "styled-components"
import Menu from "@react/menu"
import checkgreen from'../assets/images/icon/checkgreen.png'
import close from'../assets/images/icon/close.png'
import refresh from'../assets/images/icon/refresh.png'
import checkred from'../assets/images/icon/checkred.png'
import bcard from '../assets/images/backgroundcard.jpg'
import { Card } from "../../components/Card"
import Products from "../../components/Products"
import gracie from '../assets/images/product1.jpeg'
import isolate from '../assets/images/product2.png'
import cart from'../assets/images/icon/cart.png'
import menu from'../assets/images/icon/menu.png'
import search from'../assets/images/icon/search.png'
import { Menuicon } from "../../components/Menuicon"
const Container = styled.div`
  display: flex;
flex-direction: column;
align-items: center;
justify-content: center;

`
export const Home = ():JSX.Element => {
  const options:string[]=[
    "Price low to high",
    "Price high to low",
    "popularity"
  ]
  const optionMenu:string[]=['proteins','pre order','others'];
  const optionsCard =[
    {subtitle:"Whey Proteins",img:gracie},
    {subtitle:"Whey Isolates",img:isolate},
  ]
  const optionsIcon={cart,menu,search}
  return (
    <Container>
      <Menuicon optionIcon={optionsIcon}/>
      <Card image={bcard} title="use code: pigi100 for rs.100 off on your first order" titlebutton="grab now"/>
      <Products colorText="black" optionCard={optionsCard} optionsMenu={optionMenu}/>
      <Menu titleHeader="" visibilidad={false} optionMenu={[]} colorText='' enabledReset={false} recursesImages={{checkgreen,close,refresh,checkred}} onAceptar={e=>console.log(e.filter(i=>i.state==true))} onCancel={()=>console.log("Seleccion Cancelada")} options={options} color={'#22d0d2'}/>
    </Container>
  )
}
