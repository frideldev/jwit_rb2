import styled from "styled-components"
import Menu from "@react/menu"
import checkgreen from'../assets/images/icon/checkgreen.png'
import close from'../assets/images/icon/close.png'
import refresh from'../assets/images/icon/refresh.png'
import checkred from'../assets/images/icon/checkred.png'
const Container=styled.div`
  
`
export const Filter = ():JSX.Element => {
  const  options:string[]= [
    "1 up nutrition",
    "asitis",
    "avvatar",
    "big muscles",
    "bpi sports",
    "bsn",
    "cellucor",
    "demin8r",
    "dimatize",
     "dinamik",
    "esn",
    "evolution nutrition"
  ]
  const optionsHeader=['by color','by size', 'by brand', 'by price'];
  return (
    <Container>
      <Menu titleHeader={'Filter'} optionMenu={optionsHeader} visibilidad={true} colorText={'white'} recursesImages={{checkgreen,close,refresh,checkred}} onAceptar={e=>console.log(e.filter(i=>i.state==true))} onCancel={()=>console.log("Seleccion Cancelada")}  options={options} color={"#ff7745"}/>
    </Container>
  )
}
