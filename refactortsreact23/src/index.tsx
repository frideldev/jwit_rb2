import styled from "styled-components";
import {Filter} from "./pages/Filter";
import {Home} from "./pages/Home";
const Container = styled.div `
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`
function App():JSX.Element {
 
  return (
    <Container>
      <Home/>
      <Filter/>
    </Container>
  )
}

export default App
