import React from 'react'
import styled from 'styled-components'
const Container =styled.div`
width: 75%;
padding-left: 25px;
display: flex;
flex-direction: column;
justify-content: center;
`
export const Title =styled.p`
margin: 0px;
color: #6d7280;
font-weight: bold;
font-size: 16px;
`
const Description =styled.p`
margin: 0px;
margin-top: 5px;
color: #bcc1c5;
font-size: 14px;
`
const Enlace =styled.a`
margin: 0px;
margin-top: 5px;
color: #3b8699;
font-size: 14px;
`
export const Body = (params) => {
  return (
   <Container>
    <Title>
      {params.title}
    </Title>
    <Description>
        {params.subtitle}
    </Description>
    <Enlace>
        {params.enlace}
    </Enlace>

   </Container>
  )
}
