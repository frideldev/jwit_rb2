import React from 'react'
import styled from "styled-components";
import ComponentC from './ComponentC'
const Container =styled.div`
width: 90%;
height: 35px;
display: flex;
flex-direction: row;
justify-content: left;
align-items: left;
`
export const Menu = ({menu}) => {
 
  return (
  <Container>
    {
      menu.map((v,i)=><ComponentC {...v}/>)
    }
  </Container>
   
  )
}