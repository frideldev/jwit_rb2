import React from "react";
import Icon from "./Icon";
import { Body } from "./Body";
import styled from "styled-components";
import flecha from "../assets/images/flecha.png";
const Container =styled.div`
height: 35px;
width: 450px;
display: flex;
flex-direction: row;
margin-top: 15px;
`
const ComponentB = (params) => {
  return (
    <Container >
     <Icon ico={params.icon} />
     <Body title={params.title} enlace={flecha}/>
    
  </Container>
  )
}
export default ComponentB;
