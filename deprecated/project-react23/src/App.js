import styled from "styled-components";
import Filtro from "./Filtro";
import Paginainicio from "./Paginainicio";

const Container = styled.div `
display: flex;
flex-direction: row;
align-items: center;
justify-content: center;
`
function App() {
  return (
    <Container>
  <Paginainicio/>
  <Filtro/>
    </Container>
  );
}

export default App;
