import React from 'react'
import styled from 'styled-components'
import bcard from './assets/images/backgroundcard.jpg'
const Container= styled.div`
   height: 150px;
   width: 350px;
   border-radius:5px;
   background-image: url(${bcard});
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: column;
`
const Title = styled.div`
color: #ff8b60;
text-transform: uppercase;
font-size: 15px;
font-weight: bold;
width: 90%;
text-align: center;
`
const Button = styled.button`
background-color: #f06f37;
text-transform: uppercase;
border: 1px solid #f06f37;
padding: 7px;
padding-left: 25px;
padding-right: 25px;
color: white;
font-weight: bold;
border-radius: 15px;
opacity: 0.8;
cursor: pointer;
`
export const Card = () => {
  return (
    <Container>
     <Title>
        use code: pigi100 for rs.100 off on your first order
     </Title>
     <Button>
           grab now
     </Button>
    </Container>
  )
}
