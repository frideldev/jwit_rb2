import checkred from'../assets/images/checkred.png'
import styled from 'styled-components'
const Container=styled.div`
border-radius: 50%;
border: 1px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: 'white';
`
export const Icon = styled.img`
height: 35px;
width: 35px;
object-fit: contain;
border-radius: 50%;
`


export const Btn = ({onChange}) => {
    const handleChange =()=>{
    
        if(typeof onChange==='function') onChange(title)
    
    }
  return (
    <Container  onClick={handleChange}>
    <Icon src={checkred} />
   </Container>
  )
}
