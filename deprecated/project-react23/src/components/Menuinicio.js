import React from 'react'
import styled from 'styled-components'
import { Icon } from './Submenu/Options'
import cart from'./assets/images/cart.png'
import menu from'./assets/images/menu.png'
import search from'./assets/images/search.png'
const Container = styled.div`
width: 430px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`
export const Menuinicio = () => {
  return (
    <Container>
    {
        [menu,search,cart].map((v,i)=>{
         return(<Icon src={v} style={{width:25}} visible={"visible"}></Icon>)
        })
    }
    </Container>
  )
}
