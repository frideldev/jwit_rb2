import React from 'react'
import styled from 'styled-components'
import { Cardproduct } from './Cardproduct'
import gracie from '../assets/images/product1.jpeg'
import isolate from '../assets/images/product2.png'
const Container=styled.div`
width: 430px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`
const Scrollcard = () => {
    const options =[
        {subtitle:"Whey Proteins",img:gracie},
        {subtitle:"Whey Isolates",img:isolate},
      ]
  return (
    <Container>
    
     {
        options.map((v,i)=><Cardproduct key={i} {...v}/>)
      }
    </Container>
  )
}

export default Scrollcard