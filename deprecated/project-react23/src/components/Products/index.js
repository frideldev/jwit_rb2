import React from 'react'
import styled from 'styled-components'
import { Menulista } from './Menulista'
import Scrollcard from './Scrollcard'
const Container=styled.div`
width: 430px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: space-between;
`
const Products = () => {
   const data=['proteins','pre order','others'];
  return (
    <Container>
     <Menulista data={data} color={'black'}/>
    <Scrollcard/>
    </Container>
  )
}

export default Products