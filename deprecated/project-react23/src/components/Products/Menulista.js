import React from 'react'
import styled from 'styled-components'
import { Title } from '../Submenu/Options'
const Container=styled.div`
display: flex;
flex-direction: row;
align-items: center;
`
export const Menulista = ({data,color}) => {
  return (
    <Container>
     {
        data.map((v,i)=>{return(
            <Title color={color}>
              {v}
            </Title>
        )})
     }
    </Container>
  )
}
