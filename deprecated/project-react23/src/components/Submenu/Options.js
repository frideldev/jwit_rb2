import React from 'react'
import styled from 'styled-components'
import { Checkbox } from '../ui/Checkbox'

const Container = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
margin-right: 25px;
align-items: center;
`
export const Title =styled.p`
color: ${props=>props.color};
font-weight: 700;
text-transform: uppercase;
margin-left: 10px;
`
export const Options = ({title,state,onChange}) => {
   
 
  return (
    <Container>
   <Title color={'white'}>
    {title}
   </Title>
   <Checkbox data={{state,onChange}}/>
    </Container>
  )
}
