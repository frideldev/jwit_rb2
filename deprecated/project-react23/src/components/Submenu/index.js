import React from 'react'
import styled from 'styled-components'
import { Options} from './Options'

import { Footer } from './Footer'
import { Header } from './Header'
const Container = styled.div`
background-color: ${props=>props.color};
border-radius: 25px;
padding-top: 15px;
padding-left: 25px;

   width: 400px;
   
`
export const Submenu = (params) => {
  const handleChange=(value)=>{
if(typeof params.onChange==='function') params.onChange(value,params.id)
  }
  const handleReset=()=>{
    if(typeof params.onReset==='function') params.onReset(params.id)
      }
      const handleOk=(value)=>{
        if(typeof params.onAceptar==='function') params.onAceptar(value,params.id)
          }
          console.log(params.header)
    return (
     
      <Container color={params.color}>
         <Header version={params.header}/>
        {
          params.options.map((v,i)=><Options key={i} {...v} onChange={handleChange}/>)
        }
        <Footer version={params.footer} onReset={handleReset} onAceptar={handleOk} />
      </Container>
    )
 
}
