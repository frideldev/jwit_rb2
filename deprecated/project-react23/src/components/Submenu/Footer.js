import React from 'react'
import styled from 'styled-components'
import checkgreen from'../assets/images/checkgreen.png'
import close from'../assets/images/close.png'
import refresh from'../assets/images/refresh.png'
import { Circulo, Icon } from './Options'
const ContainerV1 = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
margin-top: 25px;
padding-top: 15px;
padding-bottom: 15px;
border-top: 1px solid #ffff;
`
const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: row;
`
const Item =({v,onClick})=>{
  return (
    <Circulo visible={true} style={{marginTop:20, marginLeft:15, height:50,width:50}} onClick={onClick}>
    <Icon src={v} visible={true} style={{height:50,width:50}}/>
  </Circulo>
   )
}
export const Footer = ({version, onReset,onCancel,onAceptar}) => {
  const handleReset= ()=>{
    if(typeof onReset ==='function') onReset()
  }
  const handleOk= ()=>{
    if(typeof onAceptar ==='function') onAceptar()
  }
  const handleCancel= ()=>{
    if(typeof onCancel ==='function') onCancel()
  }
  switch(version){
  case 1:{
    let data=[
      {icon:close,onClick:handleCancel},
      {icon:refresh,onClick:handleReset},
      {icon:checkgreen,onClick:handleOk}
      ];
    return (
      <ContainerV1 >
           {
              data.map((v,i)=><Item v={v.icon} onClick={v.onClick}/>)
            }
    
      </ContainerV1>
    )
  }
  case 2:{
    let data=[
      {icon:close,onClick:handleCancel},
      {icon:checkgreen,onClick:handleOk}
      ];
    return (
      <Container>
           {
              data.map((v,i)=><Item v={v.icon} onClick={v.onClick}/>)
            }
    
      </Container>
    )
  }
  default: return <></>
  }
 
}
