import React from 'react'
import styled from 'styled-components'
import { Menulista } from '../Products/Menulista'
import { Title } from './Options'
const Container = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
border-bottom: 1px solid #ffff;

`
export const Header = ({version}) => {
   if(version===true){
    const data=['by color','by size', 'by brand', 'by price'];
    return (
    
        <Container>
            <Title color={'white'} >Filter</Title>
       <Menulista data={data} color={'white'}></Menulista>
        </Container>
      )
   }
    
  else return <></>
}
