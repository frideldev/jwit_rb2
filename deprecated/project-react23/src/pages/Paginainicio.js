import React, { useState } from 'react'
import styled from 'styled-components'
import { Card } from './Card'
import { Menuinicio } from '../components/Menuinicio'
import Products from './Products'
import { Submenu } from './Submenu'
const Container=styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
`
const Paginainicio = () => {
  const [data,setData]=useState([
    {
      id:1,
      color:"#22d0d2",
      options: [
        {title:"Price low to high",state:false},
        {title:"Price high to low",state:false},
        {title:"popularity",state:false}
      ],
      footer:2,
      header:false

    }
  ])
  const handleChange =(value,id)=>{
    const match = data.filter(el=>el.id===id)[0]
    const act = match.options.filter(el=>el.title===value)[0]
  const copy = [...match.options]
  const targetIndex=copy.findIndex(f=>f.title===value)
  if(targetIndex>-1){
    copy[targetIndex]={title:value,state:!act.state};
    match.options=copy;
  }
  const copyList = [...data]
  const targetIndexList=copyList.findIndex(f=>f.title===id)
  if(targetIndexList>-1){
    copy[targetIndexList]=match;
    
  }
  setData(copyList)
  const handleAcept=copyList[0].options.filter(el=>el.state===true)
  return handleAcept
  }
  const handleOk =(value,id)=>{
    console.log(handleChange(value,id))

  return handleChange(value,id)
  }
  return (
    <Container>

<Menuinicio/>
    <Card/>
    <Products/>
    {data.map((v,i)=><Submenu key={i} {...v} onChange={handleChange} onAceptar={handleOk} />)}
    </Container>
  )
}

export default Paginainicio