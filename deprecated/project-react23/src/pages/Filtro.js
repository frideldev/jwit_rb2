import React, { useState } from 'react'
import styled from 'styled-components'
import { Listado } from '../Listado'
import { Submenu } from './Submenu'
const Container=styled.div`

`
const Filtro = () => {
  const [data,setData]=useState([
    {
      id:2,
      color:"#ff7745",
      options: [
        {title:"1 up nutrition",state:false},
        {title:"asitis",state:false},
        {title:"avvatar",state:false},
        {title:"big muscles",state:false},
        {title:"bpi sports",state:true},
        {title:"bsn",state:false},
        {title:"cellucor",state:false},
        {title:"demin8r",state:false},
        {title:"dimatize",state:false},
        {title:"dinamik",state:false},
        {title:"esn",state:false},
        {title:"evolution nutrition",state:false}
      ],
      footer:1,
      header:true

    }
  ])
  const handleChange =(value,id)=>{
    const match = data.filter(el=>el.id===id)[0]
    const act = match.options.filter(el=>el.title===value)[0]
  const copy = [...match.options]
  const targetIndex=copy.findIndex(f=>f.title===value)
  if(targetIndex>-1){
    copy[targetIndex]={title:value,state:!act.state};
    match.options=copy;
  }
  const copyList = [...data]
  const targetIndexList=copyList.findIndex(f=>f.title===id)
  if(targetIndexList>-1){
    copy[targetIndexList]=match;
    
  }
  setData(copyList) 
  const handleAcept=copyList[0].options.filter(el=>el.state===true)
  return handleAcept
  }
  const handleOk =(value,id)=>{
    console.log(handleChange(value,id))

  return handleChange(value,id)
  }
  const handleReset = (id)=>{
    const copy = [...data]
  const targetIndexList=copy.findIndex(f=>f.id===id)
  if(targetIndexList>-1){
    var raw=[];
    const match = data.filter(el=>el.id===id)[0].options
    for(let x of match) raw.push({title:x.title,state:false})
    copy[targetIndexList].options=raw;
    
  }
  setData(copy)
 
  }
  

  return (
    
    <Container>
      {data.map((v,i)=><Submenu key={i} {...v} onChange={handleChange} onReset={handleReset} onAceptar={handleOk}  /> )}
      <Listado ></Listado>
    </Container>
  )
}

export default Filtro