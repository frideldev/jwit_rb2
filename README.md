# Repositorio de Proyectos Curso JWIT
## Instalacion

Instalar proyecto con ts o js con npm

```bash
  npm install refactortsreact23
  cd refactortsreact23
```
## 🚀 Quien soy
Ing.de Sistemas,MBA en Direccion de Proyectos con experiencia en EduTech y HealthTech mediante aplicaciones web como moviles, actual postulante para integrar el equipo de JWIT.
## Proyectos

### Curso Basico

**Deprecated/project-react23:** Proyecto JS de los Videos 2 y 3

**Deprecated/proyectoinicial:** Proyecto JS del Video 1

**refactortsreact23:** Proyecto TS del Video 4
